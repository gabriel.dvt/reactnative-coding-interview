import React, { useEffect, useState } from 'react';
import { VirtualizedList } from 'react-native';
import { LoadingIndicator, SafeAreaView } from '../../components';
import Employee from '../../components/Employee';
import { useListPersons } from '../../hooks/persons';
import { IEmployee } from '../../types/employee';

export function EmployeesScreen() {
  const [list, setList] = useState<IEmployee[]>([]);
  const { data, error, isLoading, refetch, fetchNextPage, isFetchingNextPage } =
    useListPersons();

  useEffect(() => {
    if (data) {
      setList(data.pages.flatMap(d => d.data));
    }
  }, [data]);

  useEffect(() => {
    if (error) {
      console.error(error);
    }
  }, [error]);

  return (
    <SafeAreaView>
      <VirtualizedList<IEmployee>
        refreshing={isLoading}
        onRefresh={refetch}
        data={list}
        initialNumToRender={15}
        renderItem={({ item }) => (
          <Employee
            item={{
              email: item.email,
              firstname: item.firstname,
              lastname: item.lastname,
              phone: item.phone,
              website: item.website,
            }}
          />
        )}
        getItemCount={() => list.length}
        getItem={(items: any, index: number) => items[index]}
        keyExtractor={(employee, index) => String(index)}
        onEndReachedThreshold={100}
        onEndReached={() => fetchNextPage()}
        ListFooterComponent={
          isLoading || isFetchingNextPage ? <LoadingIndicator /> : <></>
        }
      />
    </SafeAreaView>
  );
}
